﻿using MessengerApp.Domain;

namespace MessengerApp.LoginOperation
{
    public interface ISessionOparation
    {
        public string Create(int userID);
        public OperationResult Delete(SessionNode sessionNode);
        public OperationResult CheckSession(SessionNode sessionNode);
    }
}