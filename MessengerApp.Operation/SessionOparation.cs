﻿using MessengerApp.DatabaseConnection;
using MessengerApp.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace MessengerApp.LoginOperation
{
    public class SessionOparation : ISessionOparation
    {
        private readonly IDatabaseOperation databaseOperation;

        public SessionOparation(IDatabaseOperation databaseOperation)
        {
            this.databaseOperation = databaseOperation;
        }

        public string Create(int userID)
        {
            Guid guid = Guid.NewGuid();
            string session = guid.ToString();
            SessionNode sessionNode = new SessionNode();
            sessionNode.userID = userID;
            sessionNode.Session = session;
            DatabaseStatusCode statusCode = databaseOperation.CreateSession(sessionNode, DateTime.Now.AddDays(30));

            if (statusCode == DatabaseStatusCode.Success)
                return session;
            else
                return null;
        }

        public OperationResult CheckSession(SessionNode sessionNode)
        {
            if(sessionNode.userID<=0 || String.IsNullOrEmpty(sessionNode.Session))
                return new OperationResult { isSuccess = false, message = "params_error" };

            if (databaseOperation.CheckSession(sessionNode) == DatabaseStatusCode.Success)
                return new OperationResult { isSuccess = true, message = "session_is_valid" };
            else
                return new OperationResult { isSuccess = false, message = "session_is_not_valid" };
        }

        public OperationResult Delete(SessionNode sessionNode)
        {
            if (sessionNode.userID <= 0 || String.IsNullOrEmpty(sessionNode.Session))
                return new OperationResult { isSuccess = false, message = "params_error" };

            if (databaseOperation.DeleteSession(sessionNode) == DatabaseStatusCode.Success)
                return new OperationResult { isSuccess = true, message = "success_logoff" };
            else
                return new OperationResult { isSuccess = false, message = "session_is_not_valid" };
        }
    }
}
