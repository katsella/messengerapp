﻿using MessengerApp.Domain;


namespace MessengerApp.Operation
{
    public interface IUserOperation
    {
        public OperationResult Register(UserNode user);
        public LoginOperationResult Login(UserNode user);
    }
}