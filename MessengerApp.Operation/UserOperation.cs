﻿using MessengerApp.DatabaseConnection;
using MessengerApp.Domain;
using MessengerApp.LoginOperation;
using System;

namespace MessengerApp.Operation
{
    public class UserOperation : IUserOperation
    {
        private readonly IEncryptOperation encryptOperation;
        private readonly IDatabaseOperation databaseOperation;
        private readonly ISessionOparation sessionOparation;

        public UserOperation(IEncryptOperation encryptOperation, IDatabaseOperation databaseOperation, ISessionOparation sessionOparation)
        {
            this.encryptOperation = encryptOperation;
            this.databaseOperation = databaseOperation;
            this.sessionOparation = sessionOparation;
        }

        public OperationResult Register(UserNode user)
        {
            if (String.IsNullOrEmpty(user.UserName) || String.IsNullOrEmpty(user.Password) || String.IsNullOrEmpty(user.Name))
                return new OperationResult { isSuccess = false, message = "empty_params" };

            user.Password = encryptOperation.Encrypt(user.Password);

            DatabaseStatusCode status = databaseOperation.CreateUser(user);
            if (status == DatabaseStatusCode.Success)
                return new OperationResult { isSuccess = true, message = "register_success" };
            else if(status == DatabaseStatusCode.UserNameExist)
                return new OperationResult { isSuccess = false, message = "username_exist" };
            else
                return new OperationResult { isSuccess = false, message = "database_connection_error" };
        }

        public LoginOperationResult Login(UserNode user)
        {
            if (String.IsNullOrEmpty(user.UserName) || String.IsNullOrEmpty(user.Password))
                return new LoginOperationResult { isSuccess = false, message = "empty_params" };


            UserNode _dbUser = databaseOperation.GetUser(user.UserName);
            if(_dbUser == null)
                return new LoginOperationResult { isSuccess = false, message = "user_not_exist" };

            user.Password = encryptOperation.Encrypt(user.Password);
            if (String.Equals(_dbUser.Password, user.Password))
            {
                string session = sessionOparation.Create(_dbUser.UserID);
                if(!String.IsNullOrEmpty(session))
                    return new LoginOperationResult { isSuccess = true, message = "login_succuss", user_id = _dbUser.UserID, Name = _dbUser.Name, UserName = _dbUser.UserName, session = session };
                else
                    return new LoginOperationResult { isSuccess = false, message = "session_error" };
            }
            else
                return new LoginOperationResult { isSuccess = false, message = "wrong_password" };
        }

    }
}
