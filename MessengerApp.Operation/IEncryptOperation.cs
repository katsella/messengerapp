﻿namespace MessengerApp.LoginOperation
{
    public interface IEncryptOperation
    {
        public string Encrypt(string password);
    }
}