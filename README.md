![App_Photo](/uploads/9a5cc97512a9320eb351fdba33a2e676/App_Photo.png)

**Features**
- Login Page
- Register Page
- Chat Page
- Session Create, Check
- sha256 bit encryption on password
- Websocket for real time chating
- .Net Core Api for login, register, session checking,
- Server side pagging for Message History (url: Get "/api/user/getoldmessages?size=20&after=0")
- Unit Test