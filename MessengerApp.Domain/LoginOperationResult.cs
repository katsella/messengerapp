﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MessengerApp.Domain
{
    public class LoginOperationResult
    {
        public bool isSuccess { set; get; }
        public string message { set; get; }
        public int user_id { set; get; }
        public string UserName { set; get; }
        public string Name { set; get; }
        public string session { set; get; }
        
    }
}
