﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MessengerApp.Domain
{
    public class OperationResult
    {
        public bool isSuccess { set; get; }
        public string message { set; get; }
    }
}
