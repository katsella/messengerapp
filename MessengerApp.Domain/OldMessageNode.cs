﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MessengerApp.Domain
{
    public class OldMessageNode
    {
        public int Size { set; get; }

        public int After { set; get; }
    }
}
