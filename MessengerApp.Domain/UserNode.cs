﻿using System;

namespace MessengerApp.Domain
{
    public class UserNode
    {
        public int UserID { get; set; }
        public string UserName { set; get; } 
        public string Password { set; get; } 
        public string Name { set; get; } 
        public string RegisterDate { set; get; } 
    }
}
