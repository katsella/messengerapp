﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MessengerApp.Domain
{
    public class SessionNode
    {
        public int userID { set; get; }
        public string Session { set; get; }
    }
}
