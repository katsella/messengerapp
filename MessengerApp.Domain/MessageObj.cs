﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MessengerApp
{
    public class MessageObj
    {
        public string UserName { get; set; }
        public int SenderUserID { get; set; }

        public string Message { get; set; }
        public int StatusCode  { get; set; }
    }
}
