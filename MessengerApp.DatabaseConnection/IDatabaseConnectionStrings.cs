﻿namespace MessengerApp.DatabaseConnection
{
    public interface IDatabaseConnectionStrings
    {
        public string GetConnectionString();
        public string GetUnitTestConnectionString();
    }
}