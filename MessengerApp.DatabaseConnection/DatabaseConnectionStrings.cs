using System;
using System.Collections.Generic;
using System.Text;

namespace MessengerApp.DatabaseConnection
{
    public class DatabaseConnectionStrings : IDatabaseConnectionStrings
    {
        private const string CONNECTION_STRING = "Data Source=localhost;Initial Catalog=Messenger;User ID=admin;Password=admin.";
        private const string UNIT_TEST_CONNECTION_STRING = "Data Source=localhost;Initial Catalog=UnitTestMessenger;User ID=admin;Password=admin";

        public string GetConnectionString()
        {
            return CONNECTION_STRING;
        }

        public string GetUnitTestConnectionString()
        {
            return UNIT_TEST_CONNECTION_STRING;
        }
    }
}
