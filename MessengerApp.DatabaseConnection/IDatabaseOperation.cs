﻿using MessengerApp.Domain;
using System;

namespace MessengerApp.DatabaseConnection
{
    public interface IDatabaseOperation
    {
        public DatabaseStatusCode CheckDatabaseConnection();
        public UserNode GetUser(string UserName);
        public DatabaseStatusCode CreateUser(UserNode user);
        public DatabaseStatusCode CreateSession(SessionNode sessionNode, DateTime expireDate);
        public DatabaseStatusCode CheckSession(SessionNode sessionNode);
        public DatabaseStatusCode DeleteSession(SessionNode sessionNode);
        public DatabaseStatusCode AddMessage(MessageObj messageObj);
        public string GetLastMessages(int size, int offset);
    }
}