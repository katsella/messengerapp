﻿using MessengerApp.Domain;
using System;
using System.Data.SqlClient;
using System.Text;

namespace MessengerApp.DatabaseConnection
{
    public class DatabaseOperation : IDatabaseOperation
    {
        IDatabaseConnectionStrings databaseConnectionStrings;
        public DatabaseOperation(IDatabaseConnectionStrings databaseConnectionStrings)
        {
            this.databaseConnectionStrings = databaseConnectionStrings;
        }

        public DatabaseStatusCode CheckDatabaseConnection()
        {
            using (SqlConnection connection = new SqlConnection(databaseConnectionStrings.GetConnectionString()))
            {
                try
                {
                    connection.Open();
                    connection.Close();
                    return DatabaseStatusCode.Success;
                }
                catch { return DatabaseStatusCode.DatabaseConnectionError; }
            }
        }

        public DatabaseStatusCode CreateUser(UserNode user)
        {
            string strCommand = "INSERT INTO dbo.Users(UserName, Password, Name, RegisterDate) VALUES('" + user.UserName + "', '" + user.Password + "', '" + user.Name + "', '" + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss") + "');";

            try
            {
                using (SqlConnection connection = new SqlConnection(databaseConnectionStrings.GetConnectionString()))
                {
                    SqlCommand command = new SqlCommand(strCommand, connection);
                    command.Connection.Open();
                    try
                    {
                        command.ExecuteNonQuery();
                        return DatabaseStatusCode.Success;
                    }
                    catch (SqlException) {return DatabaseStatusCode.UserNameExist; }
                }
            }
            catch { return DatabaseStatusCode.DatabaseConnectionError; }
        }

        public UserNode GetUser(string UserName)
        {
            string strCommand = "SELECT userID, UserName, Password, Name, RegisterDate FROM dbo.Users WHERE UserName='"+UserName+"';";

            UserNode _dbUser = new UserNode();
            try
            {
                using (SqlConnection connection = new SqlConnection(databaseConnectionStrings.GetConnectionString()))
                {
                    SqlCommand command = new SqlCommand(strCommand, connection);
                    command.Connection.Open();

                    using (SqlDataReader dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            _dbUser.UserID = dataReader.GetInt32(0);
                            _dbUser.UserName = dataReader.GetString(1);
                            _dbUser.Password = dataReader.GetString(2);
                            _dbUser.Name = dataReader.GetString(3);
                            _dbUser.RegisterDate = dataReader.GetDateTime(4).ToString();
                        }

                        connection.Close();
                    }
                    
                }
                return _dbUser;
            }
            catch { return null; }
        }


        public DatabaseStatusCode CreateSession(SessionNode sessionNode, DateTime expireDate)
        {
            string strCommand = "INSERT INTO dbo.Session(userID, session, expireDate) VALUES(" + sessionNode.userID + ",'" + sessionNode.Session + "','" + expireDate.ToString("MM/dd/yyyy hh:mm:ss") + "');";

            try
            {
                using (SqlConnection connection = new SqlConnection(databaseConnectionStrings.GetConnectionString()))
                {
                    SqlCommand command = new SqlCommand(strCommand, connection);
                    command.Connection.Open();
                    try
                    {
                        command.ExecuteNonQuery();
                        return DatabaseStatusCode.Success;
                    }
                    catch (SqlException) { return DatabaseStatusCode.SessionExist; }
                }
            }
            catch { return DatabaseStatusCode.DatabaseConnectionError; }
        }

        public DatabaseStatusCode CheckSession(SessionNode sessionNode)
        {
            string strCommand = "SELECT COUNT(*) FROM dbo.Session WHERE userID =" + sessionNode.userID + " and session='" + sessionNode.Session + "';";

            try
            {
                using (SqlConnection connection = new SqlConnection(databaseConnectionStrings.GetConnectionString()))
                {
                    SqlCommand command = new SqlCommand(strCommand, connection);
                    try
                    {
                        command.Connection.Open();
                        using (SqlDataReader dataReader = command.ExecuteReader())
                        {
                            int result = 0;
                            while (dataReader.Read())
                            {
                                result = dataReader.GetInt32(0);
                                if (result == 1)
                                    return DatabaseStatusCode.Success;
                                else
                                    return DatabaseStatusCode.SessionNotExist;
                            }
                            connection.Close();
                        }
                        return DatabaseStatusCode.SessionNotExist;
                    }
                    catch (SqlException) { return DatabaseStatusCode.DatabaseConnectionError; }
                }
            }
            catch { return DatabaseStatusCode.DatabaseConnectionError; }
        }

        public DatabaseStatusCode DeleteSession(SessionNode sessionNode)
        {
            string strCommand = "DELETE FROM dbo.Session WHERE userID =" + sessionNode.userID + " and session='" + sessionNode.Session + "';";

            try
            {
                using (SqlConnection connection = new SqlConnection(databaseConnectionStrings.GetConnectionString()))
                {
                    SqlCommand command = new SqlCommand(strCommand, connection);
                    command.Connection.Open();
                    try
                    {
                        if (command.ExecuteNonQuery() != 0)
                            return DatabaseStatusCode.Success;
                        else
                            return DatabaseStatusCode.SessionNotExist;
                    }
                    catch (SqlException) { return DatabaseStatusCode.SessionNotExist; }
                }
            }
            catch { return DatabaseStatusCode.DatabaseConnectionError; }
        }

        public DatabaseStatusCode AddMessage(MessageObj messageObj)
        {
            string strCommand = "INSERT INTO dbo.Message(senderUserID, message, sendingTime) VALUES("+messageObj.SenderUserID+", '"+ messageObj.Message +"', '"+ DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss") + "');";

            try
            {
                using (SqlConnection connection = new SqlConnection(databaseConnectionStrings.GetConnectionString()))
                {
                    SqlCommand command = new SqlCommand(strCommand, connection);
                    command.Connection.Open();
                    try
                    {
                        if (command.ExecuteNonQuery() != 0)
                            return DatabaseStatusCode.Success;
                        else
                            return DatabaseStatusCode.SessionNotExist;
                    }
                    catch (SqlException) { return DatabaseStatusCode.SessionNotExist; }
                }
            }
            catch { return DatabaseStatusCode.DatabaseConnectionError; }
        }

        public string GetLastMessages(int size, int offset)
        {
            string strCommand="";
            if (offset==0)
                strCommand = "SELECT message.messageNo, sender.userID, sender.UserName, message.message, message.sendingTime FROM dbo.Message AS message INNER JOIN dbo.Users AS sender ON (sender.userID=message.senderUserID) Order By messageNo DESC OFFSET 0 ROWS FETCH FIRST " + size+" ROWS ONLY FOR JSON AUTO;";
            else
                strCommand = "SELECT message.messageNo, sender.userID, sender.UserName, message.message, message.sendingTime FROM dbo.Message AS message INNER JOIN dbo.Users AS sender ON (sender.userID=message.senderUserID) where messageNo<" + offset+" Order By messageNo DESC OFFSET 0 ROWS FETCH FIRST " + size + " ROWS ONLY FOR JSON AUTO;";


            try
            {
                using (SqlConnection connection = new SqlConnection(databaseConnectionStrings.GetConnectionString()))
                {
                    SqlCommand command = new SqlCommand(strCommand, connection);
                    command.Connection.Open();

                    try
                    {
                        string data = "";
                        using (SqlDataReader dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                data+= dataReader.GetString(0);
                            }
                            connection.Close();
                            return data;
                        }
                    }
                    catch (SqlException) { return ""; }
                }
            }
            catch { return ""; }
        }
    }
}
