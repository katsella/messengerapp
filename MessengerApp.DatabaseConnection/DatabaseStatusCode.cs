﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MessengerApp.DatabaseConnection
{
    public enum DatabaseStatusCode
    {
        Success,
        DatabaseConnectionError,
        UserNameNotExist,
        UserNameExist,
        SessionNotExist,
        SessionExist
    }
}
