using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MessengerApp.DatabaseConnection;
using MessengerApp.LoginOperation;
using MessengerApp.Operation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace MessengerApp
{
    public class Startup {

        DatabaseConnectionStrings databaseConnectionStrings;
        DatabaseOperation databaseOperation;
        public const int LAST_MESSAGE_SIZE = 20;

        public Startup(IConfiguration configuration)
        {
            databaseConnectionStrings = new DatabaseConnectionStrings();
            databaseOperation = new DatabaseOperation(databaseConnectionStrings);
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IUserOperation, UserOperation>();
            services.AddSingleton<IEncryptOperation, EncryptOperation>();
            services.AddSingleton<IDatabaseOperation, DatabaseOperation>();
            services.AddSingleton<ISessionOparation, SessionOparation>();
            services.AddSingleton<IDatabaseConnectionStrings, DatabaseConnectionStrings>();
            services.AddControllers();
        }


        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseDefaultFiles();

            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            /*      app.UseEndpoints(endpoints =>
                  {
                      endpoints.MapControllers();
                  });*/
            app.UseEndpoints(endpoints => {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=index}/{id?}"
                    );
            });

            var webSocketOptions = new WebSocketOptions()
            {
                KeepAliveInterval = TimeSpan.FromSeconds(120),
                ReceiveBufferSize = 4 * 1024
            };

            app.UseWebSockets(webSocketOptions);
            app.Use(async (context, next) =>
            {
                if (context.Request.Path == "/messenger")
                {
                    if (context.WebSockets.IsWebSocketRequest)
                    {
                        using (WebSocket webSocket = await context.WebSockets.AcceptWebSocketAsync())
                        {
                            await Echo(context, webSocket);
                        }
                    }
                    else
                    {
                        context.Response.StatusCode = 400;
                    }
                }
                else
                {
                    await next();
                }

            });
        }

        List<WebSocket> webSocketsList = new List<WebSocket>();

        private async Task Echo(HttpContext context, WebSocket webSocket)
        {
            MessageObj messageObj = new MessageObj();
            string data;
            int userID=0;
            ArraySegment<byte> dataSegment;
            if (!int.TryParse(context.Request.Cookies["user_id"], out userID))
            {
                messageObj.StatusCode = -100;
                messageObj.SenderUserID = 0;
                messageObj.UserName = "server";
                messageObj.Message = "params_error";
                data = JsonConvert.SerializeObject(messageObj);
                dataSegment = new ArraySegment<byte>(Encoding.UTF8.GetBytes(data));
                await webSocket.SendAsync(dataSegment, WebSocketMessageType.Text, true, CancellationToken.None);
                await webSocket.CloseAsync(WebSocketCloseStatus.ProtocolError, "params_error", CancellationToken.None);
                return;
            }

            Domain.SessionNode sessionNode = new Domain.SessionNode();
            sessionNode.Session = context.Request.Cookies["session"];
            sessionNode.userID = userID;

            if(databaseOperation.CheckSession(sessionNode) != DatabaseStatusCode.Success)
            {
                messageObj.StatusCode = -100;
                messageObj.SenderUserID = 0;
                messageObj.UserName = "server";
                messageObj.Message = "session_not_verified";
                data = JsonConvert.SerializeObject(messageObj);
                dataSegment = new ArraySegment<byte>(Encoding.UTF8.GetBytes(data));
                await webSocket.SendAsync(dataSegment, WebSocketMessageType.Text, true, CancellationToken.None);
                await webSocket.CloseAsync(WebSocketCloseStatus.ProtocolError, "params_error", CancellationToken.None);
                return;
            }

            messageObj.StatusCode = 200;
            messageObj.SenderUserID = userID;

            messageObj.UserName = context.Request.Cookies["username"];


            var buffer = new byte[1024 * 4];
            webSocketsList.Add(webSocket);
            try
            {
                WebSocketReceiveResult result;
                while (webSocket.State == WebSocketState.Open)
                {
                
                    result = await webSocket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None); // client mesaji

                    messageObj.Message = Encoding.UTF8.GetString(buffer, 0, result.Count).Trim(); 
                    if (messageObj.Message.Length > 0)
                    {
                        
                        data = JsonConvert.SerializeObject(messageObj);

                        dataSegment = new ArraySegment<byte>(Encoding.UTF8.GetBytes(data));
                        databaseOperation.AddMessage(messageObj);

                        foreach (WebSocket socket in webSocketsList)
                        {
                            try
                            {
                                if (socket != webSocket)
                                    await socket.SendAsync(dataSegment, WebSocketMessageType.Text, true, CancellationToken.None);
                            }
                            catch { }
                        }
                    }
                }
            }
            catch { }
            webSocketsList.Remove(webSocket);
          //  await webSocket.CloseAsync(result.CloseStatus.Value, result.CloseStatusDescription, CancellationToken.None);
        }
    }
}