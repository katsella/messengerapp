﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MessengerApp.DatabaseConnection;
using MessengerApp.Domain;
using MessengerApp.LoginOperation;
using MessengerApp.Operation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MessengerApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserOperation userOperation;
        private readonly ISessionOparation sessionOparation;
        IDatabaseOperation databaseOperation;

        public UserController(IUserOperation userOperation, ISessionOparation sessionOparation, IDatabaseOperation databaseOperation)
        {
            this.userOperation = userOperation;
            this.sessionOparation = sessionOparation;
            this.databaseOperation = databaseOperation;
        }

        [HttpPost("Register")]
        public OperationResult Register([FromBody]UserNode user)
        {
           return userOperation.Register(user);
        }


        [HttpPost("Login")]
        public LoginOperationResult Login([FromBody] UserNode user)
        {
            return userOperation.Login(user);
        }

        [HttpPost("CheckSession")]
        public OperationResult CheckSession([FromBody] SessionNode sessionNode)
        {
            return sessionOparation.CheckSession(sessionNode);
        }

        [HttpPost("LogOff")]
        public OperationResult LogOff([FromBody] SessionNode sessionNode)
        {
            return sessionOparation.Delete(sessionNode);
        }

        [HttpGet("GetOldMessages")]
        public string GetOldMessages([FromHeader] OldMessageNode oldMessageNode)
        {
            if (!checkSession())
                return "session_not_verified";
            if (oldMessageNode.Size < 1)
                oldMessageNode.Size = 30;
            return databaseOperation.GetLastMessages(oldMessageNode.Size, oldMessageNode.After);
        }

        public bool checkSession()
        {
            SessionNode sessionNode = new SessionNode();
            sessionNode.Session = Request.Cookies["session"];
            int userID;
            if (!int.TryParse(Request.Cookies["user_id"], out userID))
                return false;
            sessionNode.userID = userID;

            return sessionOparation.CheckSession(sessionNode).isSuccess;
        }
    }
}
