﻿var userID;
var myUserName;
var lastMessageNo = 0;
let socket;
var isConnected = false;
var cleanDisconnect = false;

$(document).ready(function () {
    connectToMessageServer();
    userID = getCookie("user_id");
    myUserName = getCookie("username");
    $('#txtName').text(myUserName);
    getOldMessages(true);
});


$('#btnSend').on('click', function (e) {
    e.preventDefault();
    var message = $('#messageBox').val();
    if (message == "")
        return;
    $('#messageBox').val("");
    socket.send(message);
    var str = '{"messageNo":-1,"message":"' + message.replaceAll("\\", "\\\\").replaceAll("\"", "\\\"") + '","sendingTime":"' + new Date()+'","sender":[{"userID":'+userID+',"UserName":"'+myUserName+'"}]}';
    printMessage(JSON.parse(str), true, true, true);

});

$('#btnLogoff').on('click', function () {
    if (isConnected) {
        cleanDisconnect = true;
        socket.close();
    }
    logout();
});

$('#messageBox').keypress(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 13) { // enter tusu
         $("#btnSend").trigger('click');

        return true;
    }
});


$('#messageBox').on('click', function () {
    scrollSmoothToBottom('chatScroll');
});


function getOldMessages(scroll) {
    fetch("/api/user/getoldmessages?size=30&after=" + lastMessageNo, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    })
        .then(response => response.json())
        .then((data) => {
            if (data === "") {
                printMoreButton(true);
                return;
            }

            var jsonData = JSON.parse(data);
           
            lastMessageNo = jsonData[jsonData.length - 1].messageNo;
            for (var i = 0; i < jsonData.length-1; i++) {
                printMessage(jsonData[i], false, false, (jsonData[i].sender[0].userID != jsonData[i+1].sender[0].userID));
            }
            printMessage(jsonData[i], false, false, true);
            console.log(jsonData[0]);
            if (jsonData.length<20)
                printMoreButton(true);
            else
                printMoreButton(false);
            if (scroll)
                scrollSmoothToBottom('chatScroll');
        })
        .catch(error => console.error('Unable to add item.', error));
}

var lastMessageSenderId = 0;

function connectToMessageServer() {
    if (isConnected) {
        cleanDisconnect = true;
        socket.close();
    }
    socket = new WebSocket("wss://" + window.location.hostname + ":" + window.location.port+"/messenger");
    isConnected = true;
    socket.onopen = function (e) {
        $('.messageBoxContainer').slideDown();
        $('.messageBoxContainer').prop("disabled", true)
    };

    socket.onmessage = function (event) {
        var data = JSON.parse(event.data);
        console.log(event.data);
        //{"UserName":"Alperen","SenderUserID":1,"Message":"dfgdfg","StatusCode":200}
        if (data.StatusCode == 200)
        {
            var message = data.Message.replaceAll("\\", "\\\\").replaceAll("\"", "\\\"")
            var str = '{"messageNo":-1,"message":"' + message + '","sendingTime":"' + new Date() + '","sender":[{"userID":' + data.SenderUserID + ',"UserName":"' + data.UserName + '"}]}';
            printMessage(JSON.parse(str), true, true, (lastMessageSenderId!=data.SenderUserID));
        }
        else if (data.StatusCode == -100) {
            clearCookie();
        }
    };

    socket.onclose = function (event) {
        var isConnected = false;
        $('.messageBoxContainer').slideUp();
        $('.messageBoxContainer').prop("disabled", false)
        if (!cleanDisconnect)
            connectToMessageServer();
        cleanDisconnect = false;
       
    };

    socket.onerror = function (error) {
    };
}


function clearCanvas() {
    $('.chat-canvas').appendTo('');
}

function printMoreButton(isEnd) {
    //<div id="loadMoreButton"><button>Daha Fazla</button></div>
    $('#loadMoreButton').remove();
    if (!isEnd)
        $('.chat-canvas').append('<div id="loadMoreButton" onclick="getOldMessages(false)"><button>Daha Fazla</button></div>');
}

function printMessage(data, scroll, append, showName) {
    var dateTime = new Date(data.sendingTime);
    var sendingTime = dateTime.getHours();
    if (sendingTime >= 12) sendingTime -= 12;
    if (sendingTime < 10) sendingTime = '0' + sendingTime;
    var minutes = dateTime.getMinutes();
    if (minutes < 10) minutes = '0' + minutes;
    sendingTime = sendingTime + ":" + minutes;

    if (data.sender[0].userID == userID) //sent message
        if (append)
            $('.chat-canvas').prepend('<div class="green_box"><span>' + data.message + '<c id="box_clock">' + sendingTime +'</c></span></div>');
        else
            $('.chat-canvas').append('<div class="green_box"><span>' + data.message + '<c id="box_clock">' + sendingTime +'</c></span></div>');
    else
        if (showName) {

            if (append)
                $('.chat-canvas').prepend('<div class="white_box" style="margin-top: 10px;"><span><p class="box_name">' + data.sender[0].UserName + '</p>' + data.message + '<c class="box_clock">' + sendingTime +'</c></span></div>');
            else
                $('.chat-canvas').append('<div class="white_box" style="margin-top: 10px;"><span><p class="box_name">' + data.sender[0].UserName + '</p>' + data.message + '<c class="box_clock">' + sendingTime +'</c></span></div>');
        }
        else {
            if (append)
                $('.chat-canvas').prepend('<div class="white_box"><span>' + data.message + '<c id="box_clock">' + sendingTime +'</c></span></div>');
            else
                $('.chat-canvas').append('<div class="white_box"><span>' + data.message + '<c id="box_clock">' + sendingTime +'</c></span></div>');
        }

    if (scroll)
        scrollSmoothToBottom('chatScroll');
    lastMessageSenderId = data.sender[0].userID;
}

function getColorWithID(userID) {
    return userID.toString(16);
}

function scrollSmoothToBottom(id) {
    var div = document.getElementById(id);
    $('#' + id).animate({
        scrollTop: div.scrollHeight - div.clientHeight
    }, 250);
}

function errorHandling() {

}