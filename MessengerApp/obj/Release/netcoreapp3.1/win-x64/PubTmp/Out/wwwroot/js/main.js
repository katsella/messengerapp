function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function checkSession() {
    if (getCookie("session") == "" || getCookie("user_id") == "") {
        window.location.pathname = "";
    }
}

(function ($) {
    "use strict";

    
    /*==================================================================
    [ Validate ]*/
    var input = $('.validate-input .input100');

    $('.validate-form').on('submit',function(e){
        e.preventDefault();
        var check = true;

        for(var i=0; i<input.length; i++) {
            if(validate(input[i]) == false){
                showValidate(input[i]);
                check=false;
            }
        }
        if(check)
        {
            fetch("/api/user/login", {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: getFormAsJson()
            })
                .then(response => response.json())
                .then((data) => {
                    console.log(data);
                    if (data.isSuccess && data.session != null && data.user_id != 0) {
                        createCookie("session",data.session,30);
                        createCookie("user_id",data.user_id,30);
                    }
                })
                .catch(error => console.error('Unable to add item.', error));

        }
        return check;
    });


    $('.validate-form .input100').each(function(){
        $(this).focus(function(){
           hideValidate(this);
        });
    });

    $('#btnLogout').on('click', function() {
        logout();
    });


    function getFormAsJson() {
        var data = $("#login_form").serialize()
        var splits = data.split("&");
        var jsonString = "{";
        jsonString += "\"UserName\":\"" + splits[0].split("=")[1]+"\",";
        jsonString += "\"Password\":\"" + splits[1].split("=")[1] + "\"}";
        return jsonString;
    }

    function createCookie(name, value, days) {
        var expires;
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toGMTString();
        }
        else {
            expires = "";
        }
        document.cookie = name + "=" + value + expires + "; path=/";
    }

    function logout() {
        createCookie("session", "", 0);
        createCookie("user_id", "", 0);
        checkSession();
    }

    function validate (input) {
        if($(input).val().trim() == ''){
            return false;
        }
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('alert-validate');
    }

})(jQuery);