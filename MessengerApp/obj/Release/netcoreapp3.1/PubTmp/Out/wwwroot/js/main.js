﻿

var input = $('.validate-input .input100');

$('.validate-login-form').on('submit', function (e) {
    e.preventDefault();
    $('#login_message').text(" ");
    
    if (checkForm()) {
        fetch("/api/user/login", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: getLoginFormAsJson()
        })
            .then(response => response.json())
            .then((data) => {
                if (data.isSuccess && data.session != null && data.user_id != 0) {
                    createCookie("session", data.session, 30);
                    createCookie("name", data.name, 30);
                    createCookie("username", data.userName, 30);
                    createCookie("user_id", data.user_id, 30);
                    window.location.pathname = "/messenger.html";
                }
                else {
                    setTimeout(function () { $('#login_message').text("*Kullanici adi yada şifre hatalı."); }, 300);
                   
                }
            })
            .catch(error => console.error('Unable to add item.', error));
    }
    //  return check;
});
$('.validate-register-form').on('submit', function (e) {
    e.preventDefault();

    if (checkForm()) {
        fetch("/api/user/register", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: getRegisterFormAsJson()
        })
            .then(response => response.json())
            .then((data) => {
                if (data.isSuccess) {

                    window.location.pathname = "";
                }
                else {
                    if (data.message == "username_exist") {
                        $("#register_message").text("*Kullanici adi daha önceden alınmış.");
                    }
                }
            })
            .catch(error => console.error('Unable to add item.', error));
    }
    //  return check;
});
function checkForm() {
    var check = true;

    for (var i = 0; i < input.length; i++) {
        if (validate(input[i]) == false) {
            showValidate(input[i]);
            check = false;
        }
    }

    return check;
}


$('.validate-form .input100').each(function () {
    $(this).focus(function () {
        hideValidate(this);
    });
});

$('#btnLogout').on('click', function () {
    logout();
});



checkSession();
function checkSession(page) {
    if (getCookie("session") != "" && getCookie("user_id") > 0) {

        fetch("/api/user/checksession", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: "{ \"userID\" : " + getCookie("user_id") + ", \"session\" : \"" + getCookie("session") + "\"}"
        })
            .then(response => response.json())
            .then((data) => {
                console.log(data);
                if (!data.isSuccess) {
                    clearCookie();
                    window.location.pathname = "";
                }
                else if (page)
                    window.location.pathname = page;
            })
            .catch(error => console.error('Unable to add item.', error));

    }
}


function clearCookie() {
    var cookies = document.cookie.split(";");
    for (var i = 0; i < cookies.length; i++)
        deleteCookie(cookies[i].split("=")[0]);
    window.location.pathname = "";
}

function deleteCookie(name) {
    createCookie(name, "", -1);
}

function createCookie(name, value, days) {
    var expires;
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    }
    else {
        expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function getLoginFormAsJson() {
    var data = $("#login_form").serialize()
    var splits = data.split("&");
    var jsonString = "{";
    jsonString += "\"UserName\":\"" + splits[0].split("=")[1] + "\",";
    jsonString += "\"Password\":\"" + splits[1].split("=")[1] + "\"}";
    return jsonString;
}

function getRegisterFormAsJson() {
    var data = $("#register_form").serialize()
    var splits = data.split("&");
    var jsonString = "{";
    jsonString += "\"UserName\":\"" + splits[0].split("=")[1] + "\",";
    jsonString += "\"Name\":\"" + splits[1].split("=")[1] + "\",";
    jsonString += "\"Password\":\"" + splits[2].split("=")[1] + "\"}";
    return jsonString;
}

function logout() {
    fetch("/api/user/logoff", {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: "{ \"userID\" : " + getCookie("user_id") + ", \"session\" : \"" + getCookie("session") + "\"}"
    })
        .then(response => response.json())
        .then((data) => {
            clearCookie();
           
        })
        .catch(error => {
            clearCookie();
        });

}

function validate(input) {
    if ($(input).val().trim() == '') {
        return false;
    }
}

function showValidate(input) {
    var thisAlert = $(input).parent();

    $(thisAlert).addClass('alert-validate');
}

function hideValidate(input) {
    var thisAlert = $(input).parent();

    $(thisAlert).removeClass('alert-validate');
}

