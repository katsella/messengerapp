﻿using MessengerApp.DatabaseConnection;
using MessengerApp.Domain;
using MessengerApp.LoginOperation;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace MessengerApp.UnitTest
{
    public class SessionOperationTest
    {
        [Fact]
        public void SessionCreate_Success()
        {
            Mock<IDatabaseOperation> mockDatabaseOperation = new Mock<IDatabaseOperation>();
            mockDatabaseOperation.Setup(x => x.CreateSession(It.IsAny<SessionNode>(), It.IsAny<DateTime>())).Returns(DatabaseStatusCode.Success);
            SessionOparation sessionOparation = new SessionOparation(mockDatabaseOperation.Object);

            var result = sessionOparation.Create(20);

            Assert.False(String.IsNullOrEmpty(result));
        }

        [Fact]
        public void SessionCreate_Database_Error()
        {
            Mock<IDatabaseOperation> mockDatabaseOperation = new Mock<IDatabaseOperation>();
            mockDatabaseOperation.Setup(x => x.CreateSession(It.IsAny<SessionNode>(), It.IsAny<DateTime>())).Returns(DatabaseStatusCode.SessionExist);
            SessionOparation sessionOparation = new SessionOparation(mockDatabaseOperation.Object);

            var result = sessionOparation.Create(20);

            Assert.True(String.IsNullOrEmpty(result));
        }

        [Fact]
        public void SessionCheckSession_Success()
        {
            Mock<IDatabaseOperation> mockDatabaseOperation = new Mock<IDatabaseOperation>();
            mockDatabaseOperation.Setup(x => x.CheckSession(It.IsAny<SessionNode>())).Returns(DatabaseStatusCode.Success);
            SessionOparation sessionOparation = new SessionOparation(mockDatabaseOperation.Object);

            var result = sessionOparation.CheckSession(new SessionNode { userID = 20 , Session="UnitTest"});

            Assert.True(result.isSuccess);
        }

        [Fact]
        public void SessionCheckSession_Empty_UserID()
        {
            Mock<IDatabaseOperation> mockDatabaseOperation = new Mock<IDatabaseOperation>();
            mockDatabaseOperation.Setup(x => x.CheckSession(It.IsAny<SessionNode>())).Returns(DatabaseStatusCode.Success);
            SessionOparation sessionOparation = new SessionOparation(mockDatabaseOperation.Object);

            var result = sessionOparation.CheckSession(new SessionNode { Session = "UnitTest" });

            Assert.False(result.isSuccess);
        }

        [Fact]
        public void SessionCheckSession_Empty_UserName()
        {
            Mock<IDatabaseOperation> mockDatabaseOperation = new Mock<IDatabaseOperation>();
            mockDatabaseOperation.Setup(x => x.CheckSession(It.IsAny<SessionNode>())).Returns(DatabaseStatusCode.Success);
            SessionOparation sessionOparation = new SessionOparation(mockDatabaseOperation.Object);

            var result = sessionOparation.CheckSession(new SessionNode { userID=20 });

            Assert.False(result.isSuccess);
        }

        [Fact]
        public void SessionCheckSession_DatabaseError()
        {
            Mock<IDatabaseOperation> mockDatabaseOperation = new Mock<IDatabaseOperation>();
            mockDatabaseOperation.Setup(x => x.CheckSession(It.IsAny<SessionNode>())).Returns(DatabaseStatusCode.SessionNotExist);
            SessionOparation sessionOparation = new SessionOparation(mockDatabaseOperation.Object);

            var result = sessionOparation.CheckSession(new SessionNode { userID = 20 , Session = "UnitTest" });

            Assert.False(result.isSuccess);
        }

        [Fact]
        public void SessionDelete_Success()
        {
            Mock<IDatabaseOperation> mockDatabaseOperation = new Mock<IDatabaseOperation>();
            mockDatabaseOperation.Setup(x => x.DeleteSession(It.IsAny<SessionNode>())).Returns(DatabaseStatusCode.Success);
            SessionOparation sessionOparation = new SessionOparation(mockDatabaseOperation.Object);

            var result = sessionOparation.Delete(new SessionNode { userID = 20, Session = "UnitTest" });

            Assert.True(result.isSuccess);
        }

        [Fact]
        public void SessionDelete_Empty_UserID()
        {
            Mock<IDatabaseOperation> mockDatabaseOperation = new Mock<IDatabaseOperation>();
            mockDatabaseOperation.Setup(x => x.DeleteSession(It.IsAny<SessionNode>())).Returns(DatabaseStatusCode.Success);
            SessionOparation sessionOparation = new SessionOparation(mockDatabaseOperation.Object);

            var result = sessionOparation.Delete(new SessionNode {  Session = "UnitTest" });

            Assert.False(result.isSuccess);
        }

        [Fact]
        public void SessionDelete_Empty_UserName()
        {
            Mock<IDatabaseOperation> mockDatabaseOperation = new Mock<IDatabaseOperation>();
            mockDatabaseOperation.Setup(x => x.DeleteSession(It.IsAny<SessionNode>())).Returns(DatabaseStatusCode.Success);
            SessionOparation sessionOparation = new SessionOparation(mockDatabaseOperation.Object);

            var result = sessionOparation.Delete(new SessionNode { userID = 20});

            Assert.False(result.isSuccess);
        }

        [Fact]
        public void SessionDelete_DatabaseError()
        {
            Mock<IDatabaseOperation> mockDatabaseOperation = new Mock<IDatabaseOperation>();
            mockDatabaseOperation.Setup(x => x.DeleteSession(It.IsAny<SessionNode>())).Returns(DatabaseStatusCode.SessionNotExist);
            SessionOparation sessionOparation = new SessionOparation(mockDatabaseOperation.Object);

            var result = sessionOparation.Delete(new SessionNode { userID = 20 , Session = "UnitTest" });

            Assert.False(result.isSuccess);
        }
    }
}
