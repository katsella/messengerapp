﻿using MessengerApp.LoginOperation;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace MessengerApp.UnitTest
{
    public class EncryptOperationTest
    {
        [Fact]
        public void Encrypt_Success()
        {
            IEncryptOperation encryptOperation = new EncryptOperation();
            Assert.Equal(encryptOperation.Encrypt("123456"), "8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92");
        }

        [Fact]
        public void Encrypt_Empty()
        {
            IEncryptOperation encryptOperation = new EncryptOperation();
            Assert.Equal(encryptOperation.Encrypt(""), "");
        }
    }
}
