﻿using MessengerApp.DatabaseConnection;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace MessengerApp.UnitTest
{
    public class DatabaseOperationTest
    {
        //---------------------------------------------------
        [Fact]
        public void Real_Database_CheckConnection()
        {
            DatabaseOperation databaseOperation = new DatabaseOperation(new DatabaseConnectionStrings()); // Real database connection check
            var result = databaseOperation.CheckDatabaseConnection();

            Assert.True(result == DatabaseStatusCode.Success);
        }
        //----------------------------------------------------

        [Fact]
        public void Database_CreateUser_Success()
        {
            Mock<IDatabaseConnectionStrings> mockDatabaseConnectionStrings = new Mock<IDatabaseConnectionStrings>();
            mockDatabaseConnectionStrings.Setup(x => x.GetConnectionString()).Returns(new DatabaseConnectionStrings().GetUnitTestConnectionString());
            DatabaseOperation databaseOperation = new DatabaseOperation(mockDatabaseConnectionStrings.Object);
            var result = databaseOperation.CreateUser(new Domain.UserNode { UserName="UnitTest", Name="UnitTest", Password= "PASS" });

            Assert.True(result == DatabaseStatusCode.UserNameExist);
        }

        [Fact]
        public void Database_GetUser_Success()
        {
            Mock<IDatabaseConnectionStrings> mockDatabaseConnectionStrings = new Mock<IDatabaseConnectionStrings>();
            mockDatabaseConnectionStrings.Setup(x => x.GetConnectionString()).Returns(new DatabaseConnectionStrings().GetUnitTestConnectionString());
            DatabaseOperation databaseOperation = new DatabaseOperation(mockDatabaseConnectionStrings.Object);
            var result = databaseOperation.GetUser("UnitTest");

            Assert.NotNull(result);
        }

        [Fact]
        public void Database_Create_Session()
        {
            Mock<IDatabaseConnectionStrings> mockDatabaseConnectionStrings = new Mock<IDatabaseConnectionStrings>();
            mockDatabaseConnectionStrings.Setup(x => x.GetConnectionString()).Returns(new DatabaseConnectionStrings().GetUnitTestConnectionString());
            DatabaseOperation databaseOperation = new DatabaseOperation(mockDatabaseConnectionStrings.Object);
            var result = databaseOperation.CreateSession(new Domain.SessionNode { userID=1, Session = "UnitTest"},DateTime.Now);

            Assert.True(result == DatabaseStatusCode.Success);
        }

        [Fact]
        public void Database_Check_Session()
        {
            Mock<IDatabaseConnectionStrings> mockDatabaseConnectionStrings = new Mock<IDatabaseConnectionStrings>();
            mockDatabaseConnectionStrings.Setup(x => x.GetConnectionString()).Returns(new DatabaseConnectionStrings().GetUnitTestConnectionString());
            DatabaseOperation databaseOperation = new DatabaseOperation(mockDatabaseConnectionStrings.Object);
            var result = databaseOperation.CheckSession(new Domain.SessionNode { userID = 1, Session = "UnitTest" });

            Assert.True(result == DatabaseStatusCode.Success);
        }

        [Fact]
        public void Database_Delete_Session()
        {
            Mock<IDatabaseConnectionStrings> mockDatabaseConnectionStrings = new Mock<IDatabaseConnectionStrings>();
            mockDatabaseConnectionStrings.Setup(x => x.GetConnectionString()).Returns(new DatabaseConnectionStrings().GetUnitTestConnectionString());
            DatabaseOperation databaseOperation = new DatabaseOperation(mockDatabaseConnectionStrings.Object);
            var result = databaseOperation.DeleteSession(new Domain.SessionNode { userID = 1, Session = "UnitTest" });

            Assert.True(result == DatabaseStatusCode.Success);
        }

        [Fact]
        public void Database_Add_Message()
        {
            Mock<IDatabaseConnectionStrings> mockDatabaseConnectionStrings = new Mock<IDatabaseConnectionStrings>();
            mockDatabaseConnectionStrings.Setup(x => x.GetConnectionString()).Returns(new DatabaseConnectionStrings().GetUnitTestConnectionString());
            DatabaseOperation databaseOperation = new DatabaseOperation(mockDatabaseConnectionStrings.Object);
            var result = databaseOperation.AddMessage(new MessageObj { SenderUserID = 1, Message = "UnitTest", UserName = "UnitTest", StatusCode = 200 });

            Assert.True(result == DatabaseStatusCode.Success);
        }

        [Fact]
        public void Database_Get_Last_Messages()
        {
            Mock<IDatabaseConnectionStrings> mockDatabaseConnectionStrings = new Mock<IDatabaseConnectionStrings>();
            mockDatabaseConnectionStrings.Setup(x => x.GetConnectionString()).Returns(new DatabaseConnectionStrings().GetUnitTestConnectionString());
            DatabaseOperation databaseOperation = new DatabaseOperation(mockDatabaseConnectionStrings.Object);
            var result = databaseOperation.GetLastMessages(5, 0);

            Assert.NotEmpty(result);
        }
    }
}
