using MessengerApp.DatabaseConnection;
using MessengerApp.Domain;
using MessengerApp.LoginOperation;
using MessengerApp.Operation;
using Moq;
using System;
using Xunit;

namespace MessengerApp.UnitTest
{
    public class UserOparationTest
    {
        [Fact]
        public void CreateUser_Seccess()
        {
            Mock<IEncryptOperation> mockEncryptOperation = new Mock<IEncryptOperation>();
            mockEncryptOperation.Setup(x => x.Encrypt("UnitTest")).Returns("Test");
            Mock<IDatabaseOperation> mockDatabaseOperation = new Mock<IDatabaseOperation>();
            mockDatabaseOperation.Setup(x => x.CreateUser(It.IsAny<Domain.UserNode>())).Returns(DatabaseStatusCode.Success);
            Mock<ISessionOparation> mockSessionOparation = new Mock<ISessionOparation>();
            UserOperation userOperation = new UserOperation(mockEncryptOperation.Object, mockDatabaseOperation.Object, mockSessionOparation.Object);

            var result = userOperation.Register(new Domain.UserNode { UserName = "UnitTest", Password = "UnitTest", Name= "UnitTest" });

            Assert.True(result.isSuccess);
        }

        [Fact]
        public void CreateUser_Empty_UserName()
        {
            Mock<IEncryptOperation> mockEncryptOperation = new Mock<IEncryptOperation>();
            mockEncryptOperation.Setup(x => x.Encrypt("UnitTest")).Returns("Test");
            Mock<IDatabaseOperation> mockDatabaseOperation = new Mock<IDatabaseOperation>();
            mockDatabaseOperation.Setup(x => x.CreateUser(It.IsAny<Domain.UserNode>())).Returns(DatabaseStatusCode.Success);
            Mock<ISessionOparation> mockSessionOparation = new Mock<ISessionOparation>();
            UserOperation userOperation = new UserOperation(mockEncryptOperation.Object, mockDatabaseOperation.Object, mockSessionOparation.Object);

            var result = userOperation.Register(new Domain.UserNode { UserName = "", Password = "UnitTest", Name = "UnitTest" });

            Assert.False(result.isSuccess);
        }

        [Fact]
        public void CreateUser_Empty_Password()
        {
            Mock<IEncryptOperation> mockEncryptOperation = new Mock<IEncryptOperation>();
            mockEncryptOperation.Setup(x => x.Encrypt("UnitTest")).Returns("Test");
            Mock<IDatabaseOperation> mockDatabaseOperation = new Mock<IDatabaseOperation>();
            mockDatabaseOperation.Setup(x => x.CreateUser(It.IsAny<Domain.UserNode>())).Returns(DatabaseStatusCode.Success);
            Mock<ISessionOparation> mockSessionOparation = new Mock<ISessionOparation>();
            UserOperation userOperation = new UserOperation(mockEncryptOperation.Object, mockDatabaseOperation.Object, mockSessionOparation.Object);

            var result = userOperation.Register(new Domain.UserNode { UserName = "UnitTest", Password = "", Name = "UnitTest" });

            Assert.False(result.isSuccess);
        }

        [Fact]
        public void CreateUser_Empty_Name()
        {
            Mock<IEncryptOperation> mockEncryptOperation = new Mock<IEncryptOperation>();
            mockEncryptOperation.Setup(x => x.Encrypt("UnitTest")).Returns("Test");
            Mock<IDatabaseOperation> mockDatabaseOperation = new Mock<IDatabaseOperation>();
            mockDatabaseOperation.Setup(x => x.CreateUser(It.IsAny<Domain.UserNode>())).Returns(DatabaseStatusCode.Success);
            Mock<ISessionOparation> mockSessionOparation = new Mock<ISessionOparation>();
            UserOperation userOperation = new UserOperation(mockEncryptOperation.Object, mockDatabaseOperation.Object, mockSessionOparation.Object);

            var result = userOperation.Register(new Domain.UserNode { UserName = "UnitTest", Password = "UnitTest", Name = "" });

            Assert.False(result.isSuccess);
        }

        [Fact]
        public void CreateUser_Database_Connection_Error()
        {
            Mock<IEncryptOperation> mockEncryptOperation = new Mock<IEncryptOperation>();
            mockEncryptOperation.Setup(x => x.Encrypt("UnitTest")).Returns("Test");
            Mock<IDatabaseOperation> mockDatabaseOperation = new Mock<IDatabaseOperation>();
            mockDatabaseOperation.Setup(x => x.CreateUser(It.IsAny<Domain.UserNode>())).Returns(DatabaseStatusCode.DatabaseConnectionError);
            Mock<ISessionOparation> mockSessionOparation = new Mock<ISessionOparation>();
            UserOperation userOperation = new UserOperation(mockEncryptOperation.Object, mockDatabaseOperation.Object, mockSessionOparation.Object);

            var result = userOperation.Register(new Domain.UserNode { UserName = "UnitTest", Password = "UnitTest", Name = "UnitTest" });

            Assert.False(result.isSuccess);
        }

        [Fact]
        public void CreateUser_Database_Username_Exist()
        {
            Mock<IEncryptOperation> mockEncryptOperation = new Mock<IEncryptOperation>();
            mockEncryptOperation.Setup(x => x.Encrypt("UnitTest")).Returns("Test");
            Mock<IDatabaseOperation> mockDatabaseOperation = new Mock<IDatabaseOperation>();
            mockDatabaseOperation.Setup(x => x.CreateUser(It.IsAny<Domain.UserNode>())).Returns(DatabaseStatusCode.UserNameExist);
            Mock<ISessionOparation> mockSessionOparation = new Mock<ISessionOparation>();
            UserOperation userOperation = new UserOperation(mockEncryptOperation.Object, mockDatabaseOperation.Object, mockSessionOparation.Object);

            var result = userOperation.Register(new Domain.UserNode { UserName = "UnitTest", Password = "UnitTest", Name = "UnitTest" });

            Assert.False(result.isSuccess);
        }


        [Fact]
        public void LoginUser_Success()
        {
            Mock<IEncryptOperation> mockEncryptOperation = new Mock<IEncryptOperation>();
            mockEncryptOperation.Setup(x => x.Encrypt("UnitTest")).Returns("UnitTest");
            Mock<IDatabaseOperation> mockDatabaseOperation = new Mock<IDatabaseOperation>();
            mockDatabaseOperation.Setup(x => x.GetUser("UnitTest")).Returns(new Domain.UserNode { UserName = "UnitTest", Password = "UnitTest", Name = "UnitTest", UserID = 20 });
            mockDatabaseOperation.Setup(x => x.CheckSession(It.IsAny<SessionNode>())).Returns(DatabaseStatusCode.Success);
            Mock<ISessionOparation> mockSessionOparation = new Mock<ISessionOparation>();
            mockSessionOparation.Setup(x => x.Create(20)).Returns("UnitTest");
            UserOperation userOperation = new UserOperation(mockEncryptOperation.Object, mockDatabaseOperation.Object, mockSessionOparation.Object);

            var result = userOperation.Login(new Domain.UserNode { UserName = "UnitTest", Password = "UnitTest"});

            Assert.True(result.isSuccess);
        }

        [Fact]
        public void LoginUser_Empty_Username()
        {
            Mock<IEncryptOperation> mockEncryptOperation = new Mock<IEncryptOperation>();
            mockEncryptOperation.Setup(x => x.Encrypt("UnitTest")).Returns("UnitTest");
            Mock<IDatabaseOperation> mockDatabaseOperation = new Mock<IDatabaseOperation>();
            mockDatabaseOperation.Setup(x => x.GetUser("UnitTest")).Returns(new Domain.UserNode { UserName = "UnitTest", Password = "UnitTest", Name = "UnitTest", UserID = 20 });
            mockDatabaseOperation.Setup(x => x.CheckSession(It.IsAny<SessionNode>())).Returns(DatabaseStatusCode.Success);
            Mock<ISessionOparation> mockSessionOparation = new Mock<ISessionOparation>();
            mockSessionOparation.Setup(x => x.Create(20)).Returns("UnitTest");
            UserOperation userOperation = new UserOperation(mockEncryptOperation.Object, mockDatabaseOperation.Object, mockSessionOparation.Object);

            var result = userOperation.Login(new Domain.UserNode { UserName = "", Password = "UnitTest" });

            Assert.False(result.isSuccess);
        }

        [Fact]
        public void LoginUser_Empty_Password()
        {
            Mock<IEncryptOperation> mockEncryptOperation = new Mock<IEncryptOperation>();
            mockEncryptOperation.Setup(x => x.Encrypt("UnitTest")).Returns("UnitTest");
            Mock<IDatabaseOperation> mockDatabaseOperation = new Mock<IDatabaseOperation>();
            mockDatabaseOperation.Setup(x => x.GetUser("UnitTest")).Returns(new Domain.UserNode { UserName = "UnitTest", Password = "UnitTest", Name = "UnitTest", UserID = 20 });
            mockDatabaseOperation.Setup(x => x.CheckSession(It.IsAny<SessionNode>())).Returns(DatabaseStatusCode.Success);
            Mock<ISessionOparation> mockSessionOparation = new Mock<ISessionOparation>();
            mockSessionOparation.Setup(x => x.Create(20)).Returns("UnitTest");
            UserOperation userOperation = new UserOperation(mockEncryptOperation.Object, mockDatabaseOperation.Object, mockSessionOparation.Object);

            var result = userOperation.Login(new Domain.UserNode { UserName = "UnitTest", Password = "" });

            Assert.False(result.isSuccess);
        }

        [Fact]
        public void LoginUser_User_Not_Exist()
        {
            Mock<IEncryptOperation> mockEncryptOperation = new Mock<IEncryptOperation>();
            mockEncryptOperation.Setup(x => x.Encrypt("UnitTest")).Returns("UnitTest");
            Mock<IDatabaseOperation> mockDatabaseOperation = new Mock<IDatabaseOperation>();
            mockDatabaseOperation.Setup(x => x.GetUser("UnitTest"));
            mockDatabaseOperation.Setup(x => x.CheckSession(It.IsAny<SessionNode>())).Returns(DatabaseStatusCode.Success);
            Mock<ISessionOparation> mockSessionOparation = new Mock<ISessionOparation>();
            mockSessionOparation.Setup(x => x.Create(20)).Returns("UnitTest");
            UserOperation userOperation = new UserOperation(mockEncryptOperation.Object, mockDatabaseOperation.Object, mockSessionOparation.Object);

            var result = userOperation.Login(new Domain.UserNode { UserName = "UnitTest", Password = "UnitTest" });

            Assert.False(result.isSuccess);
        }

        [Fact]
        public void LoginUser_Encrypted_Password_Comparison_Error()
        {
            Mock<IEncryptOperation> mockEncryptOperation = new Mock<IEncryptOperation>();
            mockEncryptOperation.Setup(x => x.Encrypt("UnitTest")).Returns("UnitTest");
            Mock<IDatabaseOperation> mockDatabaseOperation = new Mock<IDatabaseOperation>();
            mockDatabaseOperation.Setup(x => x.GetUser("UnitTest")).Returns(new Domain.UserNode { UserName = "UnitTest", Password = "UnitTest2", Name = "UnitTest", UserID = 20 });
            mockDatabaseOperation.Setup(x => x.CheckSession(It.IsAny<SessionNode>())).Returns(DatabaseStatusCode.Success);
            Mock<ISessionOparation> mockSessionOparation = new Mock<ISessionOparation>();
            mockSessionOparation.Setup(x => x.Create(20)).Returns("UnitTest");
            UserOperation userOperation = new UserOperation(mockEncryptOperation.Object, mockDatabaseOperation.Object, mockSessionOparation.Object);

            var result = userOperation.Login(new Domain.UserNode { UserName = "UnitTest", Password = "UnitTest" });

            Assert.False(result.isSuccess);
        }

        [Fact]
        public void LoginUser_Session_Null()
        {
            Mock<IEncryptOperation> mockEncryptOperation = new Mock<IEncryptOperation>();
            mockEncryptOperation.Setup(x => x.Encrypt("UnitTest")).Returns("UnitTest");
            Mock<IDatabaseOperation> mockDatabaseOperation = new Mock<IDatabaseOperation>();
            mockDatabaseOperation.Setup(x => x.GetUser("UnitTest")).Returns(new Domain.UserNode { UserName = "UnitTest", Password = "UnitTest", Name = "UnitTest", UserID = 20 });
            mockDatabaseOperation.Setup(x => x.CheckSession(It.IsAny<SessionNode>())).Returns(DatabaseStatusCode.Success);
            Mock<ISessionOparation> mockSessionOparation = new Mock<ISessionOparation>();
            mockSessionOparation.Setup(x => x.Create(20));
            UserOperation userOperation = new UserOperation(mockEncryptOperation.Object, mockDatabaseOperation.Object, mockSessionOparation.Object);

            var result = userOperation.Login(new Domain.UserNode { UserName = "UnitTest", Password = "UnitTest" });

            Assert.False(result.isSuccess);
        }
    }
}
